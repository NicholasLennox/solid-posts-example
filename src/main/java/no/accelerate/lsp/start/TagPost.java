package no.accelerate.lsp.start;

import no.accelerate.srp.start.Database;

public class TagPost extends Post {
    @Override
    public void createPost(Database db, String text) throws Exception {
        db.addAsTag(text);
    }
}
