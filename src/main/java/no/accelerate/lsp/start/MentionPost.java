package no.accelerate.lsp.start;

import no.accelerate.srp.start.Database;

public class MentionPost extends Post {

    public void createMentionPost(Database db, String text) throws Exception {
        // Extra step in mention post to notify users.
        // We have done OCP, because its extended
        // But it's not part of the Post class, so it fails lsp
        String user = parseUser(text);
        db.notifyUser(user);
        super.createPost(db, text);
    }

    private String parseUser(String text) {
        return "Username";
    }
}
