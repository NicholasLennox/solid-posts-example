package no.accelerate.lsp.start;

import no.accelerate.srp.start.Database;
import java.util.List;

public class PostHandler {
    Database db = new Database();

    public void handlePosts() throws Exception {
        List<String> newPosts = db.getUnhandledPosts();
        for (String postText: newPosts) {
            // Going to refer to the parent and use polymorphism to invoke
            // the right version of create post.
            Post post;
            if(postText.startsWith("#")) {
                post = new TagPost();
            } else if (postText.startsWith("@"))
            {
                post = new MentionPost();
            }
            else {
                post = new Post();
            }
            // If it was a MentionPost,
            // it wouldn't call the correct method and no one would be notified
            post.createPost(db, postText);
        }
    }
}
