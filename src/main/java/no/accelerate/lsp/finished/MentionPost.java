package no.accelerate.lsp.finished;

import no.accelerate.lsp.start.Post;
import no.accelerate.srp.start.Database;

public class MentionPost extends Post {
    // Now we override the createPost, so it suits lsp.
    @Override
    public void createPost(Database db, String text) throws Exception {
        String user = parseUser(text);
        db.notifyUser(user);
        super.createPost(db, text);
    }

    private String parseUser(String text) {
        return "Username";
    }
}
