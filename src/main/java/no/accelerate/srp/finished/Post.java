package no.accelerate.srp.finished;

import no.accelerate.srp.start.Database;

public class Post {
    ErrorLogger logger = new ErrorLogger();
    public void createPost(Database db, String text) {
        try {
            db.add(text);
        } catch (Exception e) {
            // The responsibility is deferred
            logger.logToFile(e.getMessage());
        }
    }
}
