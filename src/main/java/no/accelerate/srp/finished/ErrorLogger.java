package no.accelerate.srp.finished;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ErrorLogger {
    // The responsibility or error logging is encapsulated here.
    public void logToFile(String errorText) {
        try {
            Files.write(Paths.get("Errors.txt"), errorText.getBytes());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
