package no.accelerate.srp.start;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Post {
    public void createPost(Database db,  String text) {
        try {
            db.add(text);
        } catch (Exception e) {
            // This is the responsibility of error logging
            try {
                Files.write(Paths.get("Errors.txt"), e.getMessage().getBytes());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
