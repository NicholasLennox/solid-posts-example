package no.accelerate.ocp.finished;

import no.accelerate.srp.start.Database;

public class TagPost extends Post {
    // The extended behaviour is defined in the child now.
    // We can keep going with new types of posts,
    // and our Post class doesn't need to change.
    @Override
    public void createPost(Database db, String text) throws Exception {
        db.addAsTag(text);
    }
}
