package no.accelerate.ocp.finished;

import no.accelerate.srp.start.Database;

public class Post {
    public void createPost(Database db, String text) throws Exception {
        db.add(text);
    }
}
