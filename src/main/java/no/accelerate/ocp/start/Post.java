package no.accelerate.ocp.start;

import no.accelerate.srp.start.Database;

public class Post {
    public void createPost(Database db, String text) throws Exception {
        // If we have more kinds of posts in the future,
        // we have to modify this class.
        // It shouldn't be modified, but extended.
        if (text.startsWith("#"))
        {
            db.addAsTag(text);
        }
        else
        {
            db.add(text);
        }
    }
}
