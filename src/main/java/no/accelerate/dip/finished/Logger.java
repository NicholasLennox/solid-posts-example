package no.accelerate.dip.finished;

public interface Logger {
    public void log(String text);
}
