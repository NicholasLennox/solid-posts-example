package no.accelerate.dip.finished;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileErrorLogger implements Logger {
    @Override
    public void log(String text) {
        try {
            Files.write(Paths.get("Errors.txt"), text.getBytes());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
