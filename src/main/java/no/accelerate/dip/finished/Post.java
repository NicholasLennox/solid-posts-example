package no.accelerate.dip.finished;

import no.accelerate.srp.start.Database;

public class Post {
    // Dependency inverted
    // Programming to abstractions
    // Dependency injection pattern
    private Logger _logger;

    public Post(Logger _logger) {
        this._logger = _logger;
    }

    public void createPost(Database db, String text) {
        try {
            db.add(text);
        } catch (Exception e) {
            _logger.log(e.getMessage());
        }
    }
}
